package com.example.sso.client.app.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientAppOneApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientAppOneApplication.class, args);
    }

}
